import QtQuick 2.12
import QtGraphicalEffects 1.12
import "BottomSheetComponents" as Parts

Rectangle {

    id: bsContainer

    property int newYpos: 90
    property int maxY: parent.height
    property bool isAnimated: false
    property var teazerData
    property var mainContentData

    y: maxY - height
    width: parent.width

    height: 0
    Behavior on y {
        enabled: isAnimated
        SpringAnimation {
            spring: 3
            damping: 0.3
            mass: 1.0
        }
    }

    Parts.TeazerContent {
        id: teazerContent
    }
    DropShadow {
        anchors.fill: teazerContent
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: teazerContent
    }

    Parts.MainContent {
        id: mainContent
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: bsContainer
        drag.axis: Drag.YAxis
        drag.minimumY: 0
        drag.maximumY: parent.maxY
        propagateComposedEvents: true
        onPressed: {
            bsContainer.isAnimated = true
        }
        onReleased: {
            if (dragArea.drag.active) {
                bsContainer.height = bsContainer.newYpos
            }
        }
    }

    onYChanged: {
        if (dragArea.drag.active) {
            height = maxY - y
        }
    }
    onTeazerDataChanged: {
        teazerContent.text = teazerData
        height = childrenRect.height
    }

    function toggleHeight() {
        height = height === maxY ? newYpos : maxY
    }
}
