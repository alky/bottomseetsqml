import QtQuick 2.12
import QtQuick.Layouts 1.11

Rectangle {
    id: messageHolder
    width: parent.width
    height: 90
    property alias text: message.text

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 16
        RowLayout {
            id: rowInfo
            width: parent.width

            Text {
                visible: message.text !== ""
                text: "\uf1ab"
                font.family: materialicons.name
                font.pointSize: 20
                Layout.leftMargin: 16
                color: '#0363cd'
            }
            Text {
                id: message
                text: ""
                font.pixelSize: 14
                color: '#455a64'
                Layout.leftMargin: 16
                Layout.rightMargin: 16
                Layout.fillWidth: true
                wrapMode: Text.Wrap
            }
        }
    }

    Text {
        id: slideUpBtn
        text: "\uf2fc"
        font.pointSize: 20
        font.capitalization: Font.AllUppercase
        anchors.right: parent.right
        anchors.rightMargin: 16
        anchors.top: parent.top
        anchors.topMargin: 16
        color: '#455a64'
        rotation: bsContainer.height == bsContainer.maxY ? 180 : 0

        MouseArea {
            anchors.fill: parent
            onClicked: {
                bsContainer.toggleHeight()
            }
        }
    }
}
